#include <SDL.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <string.h>
#include <cmath>

using namespace std;

const int SCREEN_WIDTH = 599;
const int SCREEN_HEIGHT = 800;
const int HALF_SECOND = 1000/2;

const int EMPTY = 0;
const int BOARD_SIZE = 9;
const int TILE_INDEX[] = { 0, 3, 6 };

enum DIFFICULTY {EASY = 25, MEDIUM = 15, HARD = 10 };

void readBoard(int **board);
void printBoard(int **board);
bool isValid(int **board, int a, int b, int number);
void copyBoard(int **src, int **dest);

bool solve(int **board);
bool generatePuzzle(int **board, int difficulty);

bool init();
void cleanup();

SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;
SDL_Surface *surface = NULL;
SDL_Surface *numberImg = NULL;
SDL_Texture *numberTextures[10];
SDL_Texture *banner;

bool loadNumberImage(int number);
bool createNumberTextures();

int main( int argc, char* args[] )
{
    // Generate Puzzle
    int **board = new int*[BOARD_SIZE];
    int ** originalBoard = new int*[BOARD_SIZE];

    for (int i = 0; i < BOARD_SIZE; i++){
        board[i] = new int [BOARD_SIZE];
        originalBoard[i] = new int [BOARD_SIZE];
    }

    bool gameRunning = false;
    bool solved = false;

    int remaining = BOARD_SIZE*BOARD_SIZE;

    if(!init())
    {
        printf( "Failed to initialize!\n" );
    }
    else
    {
        bool quit = false;
        SDL_Event e;

        int wait = 0;
        bool cursorColorWhite = true;
        const int jump = 495/9;

        int row, column;
        int pressedValue;

        int cColR, cColG, cColB;
        cColR = 0x00;
        cColG = 0x00;
        cColB = 0x00;
        int cursorX1 = 52, cursorY1 = 252, cursorX2 = 52, cursorY2 = 252;

        if (!createNumberTextures()){
            cout << "Failed to load images." << endl;
            exit(3);
        }

        loadNumberImage(0);
        banner = SDL_CreateTextureFromSurface(renderer, numberImg);



        while( !quit )
        {
            pressedValue = -1;

            while( SDL_PollEvent( &e ) != 0 )
            {
                if( e.type == SDL_QUIT )
                {
                    quit = true;
                }
                else if (e.type == SDL_KEYDOWN)
                {
                    if (e.key.keysym.sym >= SDLK_1 && e.key.keysym.sym <= SDLK_9){
                        column = (cursorX1-52) / jump;
                        row = (cursorY1-252) / jump;

                        if (originalBoard[row][column] != EMPTY){
                            break;
                        }

                        pressedValue = e.key.keysym.sym - SDLK_1 + 1;

                        string s = "x";
                        s[0] = '0' + pressedValue;

                        string rr = "x", cc = "x";
                        rr[0] = '0' + row;
                        cc[0] = '0' + column;

                        string info = "Please try again. " + s + " is invalid for cell (" + rr + ", " + cc + ")." ;
                        int temp = board[row][column];
                        board[row][column] = pressedValue;

                        if (isValid(board, row, column, pressedValue) && gameRunning){
                            if (!temp) remaining--;
                            cout << "remaining: " << remaining << endl;
                            if (remaining <= 0){
                                gameRunning = false;
                                solved = true;
                                SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Hurray!", "You've successfully solved the puzzle!", window);
                            }
                        }
                        else{
                            if (gameRunning){
                                SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Sorry!", info.c_str(), window);
                                board[row][column] = temp;
                            }
                        }
                    }
                    switch(e.key.keysym.sym)
                    {
                        case SDLK_UP:
                            if (cursorY1 - jump >= 252)
                            {
                                cursorY1 -= jump;
                                cursorY2 -= jump;
                            }
                            else
                            {
                                cursorY1 = cursorY2 = 252 + jump*8;
                            }
                            break;
                        case SDLK_DOWN:
                            if(cursorY1 + jump <= 252 + jump*8)
                            {
                                cursorY1+= jump;
                                cursorY2+= jump;
                            }
                            else
                            {
                                cursorY1 = cursorY2 = 252;
                            }

                            break;
                        case SDLK_LEFT:
                            if(cursorX1 - jump >= 52)
                            {
                                cursorX1 -= jump;
                                cursorX2 -= jump;
                            }
                            else
                            {
                                cursorX1 = cursorX2 = 52 + jump*8;
                            }
                            break;
                        case SDLK_RIGHT:
                            if(cursorX1 < 52 + jump*8)
                            {
                                cursorX1 += jump;
                                cursorX2 += jump;
                            }
                            else
                            {
                                cursorX1 = cursorX2 = 52;
                            }
                            break;
                            /*
                            column = (cursorX1-52) / jump;
                            row = (cursorY1-252) / jump;*/
                        case SDLK_e:
                            if (!gameRunning){
                                cout << "Generating EASY puzzle. Please wait!" << endl;
                                while (!generatePuzzle(board, EASY))
                                    ;
                                printBoard(board);
                                solved = false;
                                gameRunning = true;
                                copyBoard(board, originalBoard);
                                remaining -= EASY;
                            }
                            else{
                                SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Invalid Request", "You are in the middle of a game. Please finish first to start a new game!", window);
                            }
                            break;
                        case SDLK_m:
                            if (!gameRunning){
                                cout << "Generating MEDIUM puzzle. Please wait!" << endl;
                                while (!generatePuzzle(board, MEDIUM))
                                    ;
                                copyBoard(board, originalBoard);
                                gameRunning = true;
                                solved = false;
                                remaining -= MEDIUM;
                            }
                            else{
                                SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Invalid Request", "You are in the middle of a game. Please finish first to start a new game!", window);
                            }
                            break;
                        case SDLK_h:
                            if (!gameRunning){
                                cout << "Generating HARD puzzle. Please wait!" << endl;
                                while (!generatePuzzle(board, HARD))
                                    ;
                                solved = false;
                                gameRunning = true;
                                copyBoard(board, originalBoard);
                                remaining -= HARD;
                            }
                            else{
                                SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Invalid Request", "You are in the middle of a game. Please finish first to start a new game!", window);
                            }
                            break;
                        case SDLK_s:
                            if(gameRunning)
                            {
                                cout << "Solve started" << endl;
                                gameRunning = false;
                                solve(originalBoard);
                                copyBoard(originalBoard, board);
                                solved = true;
                                cout << "Solving completed" << endl;
                                printBoard(board);
                                remaining = BOARD_SIZE * BOARD_SIZE;
                            }
                            else
                            {
                                SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Invalid Request", "Please start a game first! Press e (for easy), or m (for medium) or h (for hard) to start a new game!", window);

                            }
                            break;

                    }
                }
            }

            //Clear screen
            SDL_SetRenderDrawColor( renderer, 0xC0, 0xC0, 0xFF, 0xFF );
            SDL_RenderClear( renderer );

            SDL_Rect fillRect = { 52, 200, 495, 495 };
            SDL_SetRenderDrawColor(renderer, 0xC0, 0xC0, 0xC0, 0xFF );
            SDL_RenderFillRect( renderer, &fillRect );

            int x1 = 52, x2 = 52, y1 = 200, y2 = 200+495;

            for (int i = 0; i <= 9; i++)
            {
                if (i==3 || i == 6 || i == 0 || i == 9)
                {
                    SDL_SetRenderDrawColor(renderer, 0x00, 0x4C, 0x99, 0xFF );
                }
                else
                    SDL_SetRenderDrawColor(renderer, 0x00, 0x99, 0x4C, 0xFF );

                SDL_RenderDrawLine(renderer, x1, y1, x2, y2);

                x1 += jump;
                x2 += jump;
            }

            x1 = 52;
            x2 = 52+495;
            y1 = 200;
            y2 = 200;

            for (int i = 0; i <= 9; i++)
            {
                if (i==3 || i == 6 || i == 0 || i == 9)
                {
                    SDL_SetRenderDrawColor(renderer, 0x00, 0x4C, 0x99, 0xFF );
                }
                else
                    SDL_SetRenderDrawColor(renderer, 0x00, 0x99, 0x4C, 0xFF );

                SDL_RenderDrawLine(renderer, x1, y1, x2, y2);

                y1 += jump;
                y2 += jump;
            }


            wait++;
            if (wait >= 100)
            {
                if (cursorColorWhite)
                {
                    cColR = 0x00;
                    cColG = 0x00;
                    cColB = 0x00;
                }
                else
                {
                    cColR = 0xC0;
                    cColG = 0xC0;
                    cColB = 0xFF;
                }

                cursorColorWhite = !cursorColorWhite;
                wait = 0;
            }
            // +6 -6 +46 -6
            SDL_SetRenderDrawColor(renderer, cColR, cColG, cColB, 0xFF);
            SDL_RenderDrawLine(renderer, cursorX1 + 6, cursorY1 - 6, cursorX2 + 46, cursorY2 - 6);

            if (gameRunning){
                for (int i = 0; i < BOARD_SIZE; i++){
                    for (int j = 0; j < BOARD_SIZE; j++){
                        if (board[i][j]){
                            SDL_Rect numberRect = {52 + j*jump + 10, 200 + 7 + i*jump, 32, 32 };
                            SDL_RenderCopy(renderer, numberTextures[board[i][j]], NULL, &numberRect);
                        }
                    }
                }
            }

            if (solved){
                for (int i = 0; i < BOARD_SIZE; i++){
                    for (int j = 0; j < BOARD_SIZE; j++){
                        if (board[i][j]){
                            SDL_Rect numberRect = {52 + j*jump + 10, 200 + 7 + i*jump, 32, 32 };
                            SDL_RenderCopy(renderer, numberTextures[board[i][j]], NULL, &numberRect);
                        }
                    }
                }
            }

            SDL_Rect bRect = { 50, 25, 500, 150};
            SDL_RenderCopy(renderer, banner, NULL, &bRect);

            SDL_RenderPresent(renderer);
        }

    }

    cleanup();

    return 0;
}


bool init()
{
    bool success = true;

    if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
    {
        printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
        success = false;
    }
    else
    {
        window = SDL_CreateWindow( "Sudoku", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
        if( window == NULL )
        {
            printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
            success = false;
        }
        else
        {
            renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
            if(renderer == NULL)
            {
                printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
                success = false;
            }
            else
            {
                SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF );
            }

            surface = SDL_GetWindowSurface(window);
            if (surface == NULL){
                printf("Surface could not be created! SDL Error %s\n", SDL_GetError());
                success = false;
            }
        }
    }

    return success;
}

bool loadNumberImage(int number){
    string filename = "x.bmp";
    filename[0] = '0' + number;

    numberImg = SDL_LoadBMP(filename.c_str());
    if (numberImg == NULL){
        printf("Unable to load image! SDL Error: %s\n", SDL_GetError());
        return false;
    }

    return true;
}

bool createNumberTextures(){
    for (int i = 1; i <= 9; i++){
        if (!loadNumberImage(i)){
            return false;
        }

        numberTextures[i] = SDL_CreateTextureFromSurface(renderer, numberImg);
        if (numberTextures[i] == NULL){
            return false;
        }
        SDL_FreeSurface(numberImg);
    }

    return true;
}


void cleanup()
{
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);

    window = NULL;
    renderer = NULL;

    SDL_Quit();
}


bool generatePuzzle(int **board, int difficulty) {
	srand(time(NULL));

	for (int i = 0; i < BOARD_SIZE; i++) {
		for (int j = 0; j < BOARD_SIZE; j++) {
			board[i][j] = EMPTY;
		}
	}

	int counter = 0;
	while (counter < difficulty) {
		int x, y, n;
		x = rand() % 9;
		y = rand() % 9;
		n = (rand() % 9) + 1;

		if (board[x][y] != 0) continue;

		board[x][y] = n;
		if (isValid(board, x, y, n)) {
			counter++;
		} else {
			board[x][y] = 0;
		}
	}

	int **t = new int*[BOARD_SIZE];
	for (int i = 0; i < BOARD_SIZE; i++) {
		t[i] = new int[BOARD_SIZE];
		for (int j = 0; j < BOARD_SIZE; j++) {
			t[i][j] = board[i][j];
		}
	}

	bool ok = solve(t);
	for (int i = 0; i < BOARD_SIZE; i++)
		delete[] t[i];
	delete[] t;

	cout << "Generating puzzle! Please wait. Counter: " << counter << endl;

	return ok;
}

bool solve(int **board) {
	int a = -1, b = -1;
	for (int i = 0; i < BOARD_SIZE; i++) {
		for (int j = 0; j < BOARD_SIZE; j++) {
			if (board[i][j] == EMPTY) {
				a = i;
				b = j;
				break;
			}
		}
		if (a != -1)
			break;
	}

	if (a == -1)
		return true;

	for (int k = 1; k <= 9; k++) {
		board[a][b] = k;

		if (isValid(board, a, b, k)) {
			if (solve(board))
				return true;
		}

		board[a][b] = 0;
	}

	return false;
}

void printBoard(int **board) {
	for (int i = 0; i < BOARD_SIZE; i++) {
		for (int j = 0; j < BOARD_SIZE; j++) {
			cout << board[i][j] << " ";
		}
		cout << endl;
	}
}

void readBoard(int** board) {
	for (int i = 0; i < BOARD_SIZE; i++) {
		board[i] = new int[BOARD_SIZE];
		for (int j = 0; j < BOARD_SIZE; j++) {
			cin >> board[i][j];
		}
	}
}

bool isValid(int **board, int a, int b, int number) {
	if (number < 1 || number > 9)
		return false;

	int *values = new int[BOARD_SIZE + 1];

	for (int i = 0; i < BOARD_SIZE + 1; i++) {
		values[i] = 0;
	}

//row check
	for (int j = 0; j < BOARD_SIZE; j++) {
		int value = board[a][j];
		if (value == EMPTY)
			continue;

		values[value]++;
		if (values[value] > 1) {
			delete[] values;

			return false;
		}
	}

	for (int i = 0; i < BOARD_SIZE + 1; i++) {
		values[i] = 0;
	}

//column check
	for (int j = 0; j < BOARD_SIZE; j++) {
		int value = board[j][b];
		if (value == EMPTY)
			continue;
		values[value]++;
		if (values[value] > 1) {
			delete[] values;
			return false;
		}
	}

	for (int i = 0; i < BOARD_SIZE + 1; i++) {
		values[i] = 0;
	}

	int rowIndex = TILE_INDEX[a / 3];
	int colIndex = TILE_INDEX[b / 3];

	for (int i = rowIndex; i < rowIndex + 3; i++) {
		for (int j = colIndex; j < colIndex + 3; j++) {
			int value = board[i][j];
			if (value == EMPTY)
				continue;

			values[value]++;
			if (values[value] > 1) {
				delete[] values;
				return false;
			}

		}
	}

	delete[] values;
	return true;
}

void copyBoard(int **src, int **dest){
    for (int i = 0; i < BOARD_SIZE; i++){
        for (int j = 0; j < BOARD_SIZE; j++){
            dest[i][j] = src[i][j];
        }
    }
}
 
