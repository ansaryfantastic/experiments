/*
	The client program.
	
	1) catalog: 	type on terminal: ./client localhost catalog
	2) dir:		type on terminal: ./client host dir
	3) dcopy:	type on terminal: ./client host dcopy source destination
	4) ucopy:	type on terminal: ./client host ucopy source destination
	
	Most of the code here is similar to the server's code. See the server code for details
	
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include <iostream>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <fcntl.h>

#include <netinet/in.h>
#include <netdb.h>

#include <arpa/inet.h>
#include <signal.h>

#include <dirent.h>

#define MAXSTR 	1025
#define BUFFER  2048
#define BACKLOG 10
#define BUFFER 	2048

#define PORT    "2001"


using namespace std;

typedef enum { _DIR, _DCOPY, _UCOPY, _BYE, ACK, ERR } PacketType;
typedef struct
{
    PacketType type;
    int size;
    int perm;
    char text[MAXSTR];
} packet;


void error(const char *msg)
{
    perror(msg);
    exit(0);
}

void fileclient(int sockfd, int argc, char *argv[]);

void dcopy(int sock, char *source, char *destination);
void ucopy(int sock, char *source, char *destination);
void dir(int sockfd);
void bye(int sock);

int main(int argc, char *argv[]){
	int sockfd, numbytes;
	char buf[2048];
	struct addrinfo hints, *servinfo, *p;

	int rv;

	if (argc < 3){
		fprintf(stderr, "usage: client hostname requestType\n");
		return 1;
	}

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	if ((rv = getaddrinfo(argv[1], PORT, &hints, &servinfo)) != 0){
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		return 1;
	}

	for (p = servinfo; p != NULL; p = p->ai_next){
		if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1){
			perror("client: socket");
			continue;
		}

		if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1){
			close(sockfd);
			perror("client: connect");
			continue;
		}

		break;
	}

	if (p == NULL){
		fprintf(stderr, "client: failed to connect\n");
		return 2;
	}

    fileclient(sockfd, argc, argv);
    close(sockfd);
    return 0;
}

void fileclient(int sockfd, int argc, char *argv[]){				
	if (strcmp(argv[2], "catalog") == 0){
		DIR *dir = opendir(".");
		char temp[4086];
		memset(temp, 0, sizeof(temp));
		
		if (dir){
			struct dirent *ent;
			
			while ((ent = readdir(dir)) != NULL){
				cout << ent->d_name << endl;
			}
		}
		else cout << "Directory not read" << endl;	
				
	}
	else if (strcmp(argv[2], "dir") == 0){
		dir(sockfd);		
	}
	else if (strcmp(argv[2], "dcopy") == 0){
		dcopy(sockfd, argv[3], argv[4]);
	}
	else if (strcmp(argv[2], "ucopy") == 0){
		ucopy(sockfd, argv[3], argv[4]);
	}
	else if (strcmp(argv[2], "bye") == 0){
		cout << "bye...." << endl;
		bye(sockfd);
	}
	else cout << "unrecognised command" << endl;		

}

void dir(int sock){			// Get the directory listing from the server.
	char buffer[256];
	
	memset(&buffer, 0, sizeof(buffer));
	
	packet request, response;

	memset(&request, 0, sizeof(request));
	memset(&response, 0, sizeof(response));
	
	request.type = _DIR;	
	write(sock, &request, sizeof(request));
	
	int readBytes = read(sock, &response, sizeof(packet));
	if (readBytes < 0){
		cout << "Error reading from socket" << endl;
		return;
	}
	
	while(readBytes){
		readBytes = read(sock, buffer, 255);
		if (readBytes < 0){
			cout << "ERROR reading from socket" << endl;
			return;
		}		
		buffer[readBytes] = '\n';
		cout << buffer;
	}
}

void dcopy(int sock, char *source, char *destination){		// download a file from the server
	packet request, response;

	memset(&request, 0, sizeof(request));
	memset(&response, 0, sizeof(response));
	
	char buffer[4086];
	
	memset(buffer, 0, sizeof(buffer));
	
	int n = 1, file_to_write, mode;
	request.type = _DCOPY;
	strcpy(request.text, source);
	write(sock, &request, sizeof(request));
	
	n = read(sock, &response, sizeof(response));
	
	if (n < 0){
		cout << "ERROR reading from socket" << endl;
		exit(1);
	}
	if (response.type == ERR){
		cout << response.text << endl;
		exit(1);
	}

	mode = 5 << 6;
	
	file_to_write = open(destination, O_WRONLY|O_CREAT, mode);
	if (file_to_write < 0){
		cout << "Error: cannot create file" << endl;
		exit(1);
	}
	
	int count = response.size;
	cout << "Start receiving " << count << " bytes" << endl;
	
	while (count > 0){
		int bytes_wrote = 0;
		int n = read(sock, buffer, sizeof(buffer));
		if (n < 0){
			cout << "Error reading from socket" << endl;
			exit(1);
		}
		
	
		bytes_wrote = write(file_to_write, buffer, n);
		if (bytes_wrote < 0 || bytes_wrote != n){
			cout << "ERROR writing" << endl;
			exit(1);
		}
		
		count -= n;
	}
	
}

void ucopy(int sock, char *source, char *destination){		// upload a file from the server			
	packet request, response;

	memset(&request, 0, sizeof(request));
	memset(&response, 0, sizeof(response));
	
	char buffer[4086];
	
	memset(buffer, 0, sizeof(buffer));

	request.type = _UCOPY;	
	strcpy(request.text, destination);
	request.perm = 5;
	
	strcpy(buffer, source);
		
	int file_to_put = open(buffer, O_RDONLY);
	
	cout << "file_to_put: " << file_to_put << " : " << buffer << endl;
	
	struct stat info;
	stat(buffer, &info);
	request.size = info.st_size;
	cout << "request.size: " << request.size << endl;
	
	write(sock, &request, sizeof(request));
	
	cout << "Start sending " << (int)info.st_size <<" bytes" << endl;
	int count = info.st_size;
	
	while (count > 0){
		int bytes_sent;
		int n = read(file_to_put, buffer, count < 4086? count: 4086);
		cout << n << " bytes sent." << endl;
		if (n < 0){
			cout << "error sending data" << endl;
			break;
		}
		
		bytes_sent = write(sock, buffer, n);
		
		if (bytes_sent < 0 || n != bytes_sent){
			cout << "ERROR while writing to socket" << endl;
			break;
		}		
		count -= n;
	}
	
	read(sock, &response, sizeof(response));
	
	if (response.type == ACK){
		cout << "File successfully sent to server" <<endl;
	}
	else cout << "ERROR: " << response.text << endl;
			
}

void bye(int sock){				// Force the server to quit.
	packet request;
	
	memset(&request, 0, sizeof(request));
	request.type = _BYE;
	cout << "request type set to: " << request.type << endl;
	
	write(sock, &request, sizeof(request));	
	
}

















