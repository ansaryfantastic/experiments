#include <iostream>
#include <fstream>
#include <cstring>
#include <queue>
#include <vector>
#include <cstdlib>

using namespace std;

/* ************************Global Variables*************************** */

const char *MAIN_DB_FILE	= NULL;

int nFields;
int startoffset;
int avail_listoffset;
char **fieldNames			= NULL;
FILE *f;

struct struct_avail_list{
	int number_of_slots;
	int slot_offset[20];
	int slot_size[20];
};

struct_avail_list avail_list;

/* ******************************************************************* */


/* ****************Function Prototypes******************************** */

char *readRecord();
char *getCmdData(string cmd);
char *getRecordID(const char *record);
queue<unsigned long> getRecordOffset(const char *record);
void writeRecord(int record_size, bool empty_flag, const char *rec);

void Initialize();

void Select(queue<unsigned long>);
void Delete(unsigned long);
void Update(unsigned long, const char*);
void Insert(const char*);
void Join(const char*);

void mergeAdjacent(int offset, int size);

int hash(unsigned int record_id);
/* ******************************************************************* */


/* *******************************************************************
void print(){
    cout << "Number of slots: " << avail_list.number_of_slots << endl;
    for (int i = 0; i < 20; i++){
        cout << avail_list.slot_offset[i] << "  " << avail_list.slot_size[i] << endl;
    }
}
 ******************************************************************* */

int main(int argc, char **argv){
	if (argc != 2){
		cout << "ERROR: Invalid arguments. Expected ./dbe <file_name>" << endl;
		return 1;
	}
    MAIN_DB_FILE = argv[1];


	Initialize();

	while (1){
		string cmd;
		getline(cin, cmd);
		if (cmd[0] == 's'){
			char *recordID = getCmdData(cmd);

			Select(getRecordOffset(recordID));

			delete[] recordID;
		}
		else if (cmd[0] == 'd'){
			char *recordID = getCmdData(cmd);
			queue<unsigned long> q = getRecordOffset(recordID);
			if (!q.empty())
				Delete(q.front());

			delete[] recordID;
		}
		else if (cmd[0] == 'q'){
			return 0;
		}
		else if (cmd[0] == 'u'){
			char *record = getCmdData(cmd);
			char *recordID = getRecordID(record);
			queue<unsigned long> q = getRecordOffset(recordID);
			if (!q.empty())
				Update(q.front(), record);

			delete[] recordID;
			delete[] record;
		}
		else if (cmd[0] == 'i'){
			char *record = getCmdData(cmd);
			Insert(record);
			delete[] record;
		}
		else if (cmd[0] == 'j'){
			char *filename = getCmdData(cmd);
			Join(filename);
			delete[] filename;
		}
	}

	return 0;
}


void Select(queue<unsigned long> q_offset){
	while (!q_offset.empty()){
		unsigned long offset = q_offset.front();
		q_offset.pop();

		if (offset == 0){
			continue;
		}

		f = fopen(MAIN_DB_FILE, "rb");
		fseek(f, offset-1, SEEK_SET);

		char empty;
		fread(&empty, sizeof(empty), 1, f);

		if (empty == 1){
			fclose(f);
			continue;
		}

		fseek(f, offset, SEEK_SET);
		for (int i = 0; i < nFields; i++){
			char *record = readRecord();
			cout << fieldNames[i] << " = " << record << endl;
			if (record){
			    delete[] record;
			}

			record = 0;
		}
		fclose(f);
		return;
	}
	cout << "Record not found" << endl;
}

void Update(unsigned long offset, const char* newRecord){
	unsigned int oldRecordSize;

	f = fopen(MAIN_DB_FILE, "r+b");

	int len = strlen(newRecord);
	char *_newRecord = new char[len+2];

	for (int i = 0; i < len; i++){
		_newRecord[i] = newRecord[i];
	}
	_newRecord[len] = '#';
	_newRecord[len+1] = '\0';

	fseek(f, offset-5, SEEK_SET);
	fread(&oldRecordSize, sizeof(oldRecordSize), 1, f);
	fclose(f);

	unsigned int newRecordSize = strlen(newRecord) +  sizeof(unsigned int) + sizeof(char);

	if (newRecordSize <= oldRecordSize){
		f = fopen(MAIN_DB_FILE, "r+b");
		fseek(f, offset-5, SEEK_SET);
		writeRecord(newRecordSize, 0, _newRecord);
		if (_newRecord)
            delete[] _newRecord;
        _newRecord = 0;

		if (oldRecordSize-newRecordSize != 0){
			mergeAdjacent(ftell(f), oldRecordSize-newRecordSize);
		}
		fflush(f);
		fclose(f);
	}
	else{
		Delete(offset);
		Insert(newRecord);
		if (_newRecord)
            delete[] _newRecord;
        _newRecord = 0;
	}
}


void Delete(unsigned long offset){
	if (offset == 0){
		return;
	}

	f = fopen(MAIN_DB_FILE, "r+b");

	fseek(f, offset-5, SEEK_SET);

	unsigned int size;
	fread(&size, sizeof(size), 1, f);

	fseek(f, offset-1, SEEK_SET);
	char empty = 1;
	fwrite(&empty, sizeof(empty), 1, f);
	fflush(f);

	fclose(f);

	mergeAdjacent(offset-5, size);
}

void Insert(const char *rec){
	int len = strlen(rec);

	char *record = new char[len+2];



	for (int i = 0; i < len; i++){
		record[i] = rec[i];
	}

	record[len] = '#';
	record[len+1] = '\0';

	int size = strlen(record) + sizeof(unsigned int) + sizeof(char);

	if (size <= avail_list.slot_size[0]){
		f = fopen(MAIN_DB_FILE, "r+b");

		fseek(f, avail_list.slot_offset[0], SEEK_SET);
		writeRecord(size, 0, record);

		avail_list.slot_size[0] -= size;
		avail_list.slot_offset[0] = ftell(f);
		fflush(f);

		fclose(f);
		mergeAdjacent(0,0);
	}
	else{
		 f = fopen(MAIN_DB_FILE, "r+b");
		fseek(f, 0, SEEK_END);
		writeRecord(size, 0, record);
		fflush(f);
		fclose(f);
	}
	if (record)
        delete[] record;
    record = 0;
}

queue<unsigned long> getRecordOffset(const char *record){
	queue<unsigned long> q;

	f = fopen(MAIN_DB_FILE, "rb");
	char c;
	fseek(f, startoffset+5, SEEK_SET);

	char *read = readRecord();

	if (strcmp(read, record) == 0){
		fseek(f, -(strlen(read)+1), SEEK_CUR);
		if (read)
            delete[] read;
        read = 0;
		q.push(ftell(f));
	}
    if (read)
        delete[] read;
    read = 0;


	while (!feof(f)){
		fread(&c, sizeof(char), 1, f);
		if (c == '#'){
			fseek(f, 5, SEEK_CUR);
			read = readRecord();
			if (read == 0){
				continue;
			}

			//			cout << ftell(f) << ": read: " << read << endl;

			if (strcmp(read, record) == 0){
				fseek(f, -(strlen(read)+1), SEEK_CUR);
				if (read)
                    delete[] read;
				read = 0;
				q.push(ftell(f));
			}
			if (read)
                delete[] read;
            read = 0;
		}
	}
	fflush(f);
	fclose(f);

	return q;
}



char *getCmdData(string cmd){
	int i;
	i = 0;
	while (cmd[i] != ' ')
		i++;

	int len = cmd.length();
	char *recordID = new char[len - i-1];

	for (int j = i+1; j < len; j++){
		recordID[j-i-1] = cmd[j];
	}

	recordID[len-i-1] = '\0';

	return recordID;
}

void writeRecord(int record_size, bool empty_flag, const char *rec){
	fwrite(&record_size, sizeof(record_size), 1, f);
	fflush(f);
	fwrite(&empty_flag, sizeof(empty_flag), 1, f);
	fflush(f);
	fwrite(rec, sizeof(char), strlen(rec), f);
	fflush(f);
}


char *getRecordID(const char *record){
	string ret = "";
	int i = 0;
	while(record[i] != '|'){
		ret += record[i++];
	}

	char *id = new char[i+1];
	for (int j = 0; j < i; j++){
		id[j] = ret[j];
	}
	id[i] = '\0';
	return id;
}


void Initialize(){
	f = fopen(MAIN_DB_FILE, "rb");

	fread(&nFields, sizeof(int), 1, f);

	fieldNames = new char*[nFields];

	for (int i = 0; i < nFields; i++){
		char ch;
		string s = "";
		int len = 0;
		while (1){
			fread(&ch, sizeof(char), 1, f);
			if (ch == '|')
				break;
			s += ch;
			len++;
		}
		fieldNames[i] = new char[len+1];
		for (int j = 0; j < len; j++){
			fieldNames[i][j] = s[j];
		}
		fieldNames[i][len] = '\0';
	}

	avail_listoffset = ftell(f);
	fread(&avail_list, sizeof(struct_avail_list), 1, f);

	startoffset = ftell(f);
	fclose(f);
}

char *readRecord(){
	string s = "";
	int len = 0;
	while (1){
		char c;
		int bytesRead = fread(&c, sizeof(char), 1, f);

		if (bytesRead == 0){
			return 0;
		}


		if (c == '|' || c== '#')
			break;

		len++;
		s += c;
	}

	char *ret = new char[len+1];
	for (int i = 0; i < len; i++){
		ret[i] = s[i];
	}
	ret[len] = '\0';
	return ret;
}




void sortFirstColumn(int *offset, int *size){
    for (int i = 0; i < 21; i++){
        for (int j = i+1; j < 21; j++){
            if (offset[j] < offset[i]){
                swap(offset[i], offset[j]);
                swap(size[i], size[j]);
            }
        }
    }
}

void sortSecondColumn(int *offset, int *size){
    for (int i = 0; i < 21; i++){
        for (int j = i+1; j < 21; j++){
            if (size[j] > size[i]){
                swap(offset[i], offset[j]);
                swap(size[i], size[j]);
            }
        }
    }
}


void mergeAdjacent(int of, int sz){
    int offset[21];
    int size[21];

    for (int i = 0; i < 20; i++){
        offset[i] = avail_list.slot_offset[i];
        size[i] = avail_list.slot_size[i];
    }

    offset[20] = of;
    size[20] = sz;


    bool again = true;
    while (again){
        sortFirstColumn(offset, size);

        int n = 21;
        again = false;
        for (int i = 1; i < n; i++){
            if (size[i] != 0 && offset[i] <= size[i-1] + offset[i-1]){
                size[i-1] = size[i] + offset[i] - offset[i-1];
                size[i] = offset[i] = 0;
                i++;
                again = true;
            }
        }
    }

    sortSecondColumn(offset, size);

    avail_list.number_of_slots = 0;

    for (int i = 0; i < 20; i++){
        avail_list.slot_offset[i] = offset[i];
        avail_list.slot_size[i] = size[i];
        if (avail_list.slot_size[i] > 0){
            avail_list.number_of_slots++;
        }
    }

    f = fopen(MAIN_DB_FILE, "r+b");
    fseek(f, avail_listoffset, SEEK_SET);
    fwrite(&avail_list, sizeof(avail_list), 1, f);
	fflush(f);
    fclose(f);
}

struct join_header{
	unsigned int nFields;
	char fieldnames[94];
};

struct join_block{
	unsigned int nRecords;
	char record[24][100];
	unsigned int nextBlock;

};

vector<char*> columns(char *str){
	int len = strlen(str);
	vector<char*> v;
	int i = 0;
	while (i < len){
		string s = "";
		for (;i < len; i++){
			if (str[i] == '|')
				break;
			s += str[i];
		}
		int len = s.length();
		char *pb = new char[len+1];
		for (int j = 0; j < len; j++){
			pb[j] = s[j];
		}
		pb[len] = '\0';
		v.push_back(pb);
		i++;
	}
	return v;
}

int getCount(const char *filename, int targetCol, int offset, const char *record){
	int result = 0;
	join_block block;

	FILE *jf = fopen(filename, "r+b");

	fseek(jf, offset, SEEK_CUR);
	fread(&block, sizeof(block), 1, jf);
	fclose(jf);

	for (unsigned int i = 0; i < block.nRecords; i++){
		vector<char*> v = columns(block.record[i]);
		if (strcmp(v[targetCol], record) == 0){
			result++;
		}
	}

	if (block.nextBlock != 0){
		return result + getCount(filename, targetCol, block.nextBlock, record);
	}
	else return result;
}

void outputResult(const char *filename, int targetCol, const char *record){
	cout << record << " ";
	int ctr = 0;

	int h = hash(atoi(record));
	int offset = 100 + 2048*h;

	ctr = getCount(filename, targetCol, offset, record);

	cout << ctr << endl;
}

void Join(const char* filename){
	FILE *jf = fopen(filename, "rb");

	join_header header;

	fread(&header, sizeof(header), 1, jf);
	fclose(jf);


	vector<char*> v = columns(header.fieldnames);

	int targetCol = 0;

	int sz = v.size();
	for (int i = 0; i < sz; i++){
		if (strcmp(v[i], fieldNames[0]) == 0){
			targetCol = i;
			break;
		}
	}

	f = fopen(MAIN_DB_FILE, "rb");
	char c;
	fseek(f, startoffset+5, SEEK_SET);

	char *read = readRecord();
	outputResult(filename, targetCol, read);
	if (read)
        delete[] read;
    read = 0;

	while (!feof(f)){
		fread(&c, sizeof(char), 1, f);
		if (c == '#'){
			fseek(f, 5, SEEK_CUR);
			char *read = readRecord();
			if (read == 0){
				continue;
			}
			outputResult(filename, targetCol, read);
			if (read)
                delete[] read;
            read = 0;
		}
	}
	fflush(f);
	fclose(f);
}


int hash(unsigned int record_id){
	return (record_id%20);
}
