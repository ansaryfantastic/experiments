/*
	The server program.
	Run it and then run the client.
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include <iostream>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <fcntl.h>

#include <netinet/in.h>
#include <netdb.h>

#include <arpa/inet.h>
#include <signal.h>

#include <dirent.h>

#define MAXSTR 	1025
#define BUFFER  2048
#define BACKLOG 10
#define BUFFER 	2048

#define PORT    "2001"

using namespace std;

typedef enum { _DIR, _DCOPY, _UCOPY, _BYE, ACK, ERR } PacketType;

/*
	This packet is used to communicate request types and ACK and ERR between the server and the client.
	The size field is used to communicate the size of the file between the server and the client.
*/
typedef struct
{
    PacketType type;
    int size;
    int perm;
    char text[MAXSTR];
} packet;


void error(const char *msg)
{
    perror(msg);
    exit(0);
}

void fileserver(int sock);

int main(){
    int sockfd;     // listen on sockfd
    int new_fd;     // new connection on newfd

    struct addrinfo hints, *servinfo, *p;	
	struct sockaddr_storage their_addr;	// connector's address information
    socklen_t sin_size;

	char s[INET6_ADDRSTRLEN];
    int rv;

    memset(&hints, 0, sizeof hints);		// clear hints
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;		// use my ip

    if ((rv = getaddrinfo(NULL, PORT, &hints, &servinfo)) != 0){
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
        return 1;
    }

	// loop through all the results and bind to the first one we can
    for (p = servinfo; p != NULL; p = p->ai_next){
        if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1){
            perror("server: socket");
            continue;
        }

        int yes = 1;
        if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1){
            perror("setsockopt");
            return 1;
        }

        if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1){
            close(sockfd);
            perror("server: bind");
            continue;
        }
        break;
    }

    if (p == NULL){
        fprintf(stderr, "server: failed to bind\n");
        return 1;
    }

    freeaddrinfo(servinfo);		// all done with this structure

    if (listen(sockfd, BACKLOG) == -1){
        perror("listen");
        return 1;
    }

    cout << "Server: Waiting for connection" << endl;

    while(1){
    	sin_size = sizeof their_addr;
        new_fd = accept(sockfd, (struct sockaddr*)&their_addr, &sin_size);

        if (new_fd == -1){
            perror("accept");
            continue;
        }
        else cout << "Connection accepted" << endl;

        fileserver(new_fd);
        close(new_fd);
    }
}

/*
	The following function implements the required functionality. 
	The basic idea is to get a packet from the client which carries the required request.
	The server acts acording to the request type.
	
	All this happens after successful establishment of the connection.
*/

void fileserver(int sock){
	packet response, request;
	
	bzero(&request, sizeof(packet));		// clear the packets
	bzero(&response, sizeof(packet));
	
	int n;
	
	n = read(sock, &request, sizeof(packet));	// get the request type for the client
	
	if ( n < 0){
		response.type = ERR;
		strcpy("Request from client not received properly", response.text);
		n = write(sock, &response, sizeof(response));
		return;
	}
	
	response.type = ACK;
	cout << "request.type: " << request.type << endl;
	
	switch(request.type){					// take action based on the request type
		case _DIR:
			{
				cout << "DIR request" << endl;
			
				DIR *dir = opendir(".");
		
				char temp[4086];
				memset(temp, 0, sizeof(temp));
		
				if (dir){
					response.type = ACK;				// notify client of success!
					write(sock, &response, sizeof(response));			
					struct dirent *ent;				// structure used to hold directory info
			
					while ((ent = readdir(dir)) != NULL){
						strcat(temp, ent->d_name);
						strcat(temp, "\n");					
					}
					write(sock, &temp, strlen(temp)+1);		// send directory list to client
				}
				else{
					response.type = ERR;
					strcpy("Directory not read", response.text);
					write(sock, &response, sizeof(response));
				}
				break;
			}
						
		case _DCOPY:
				{
					cout << "DCOPY request" << endl;
					
					int file_to_get, bytes_read;
					
					char file_data[4086];					
					struct stat info;
					stat(request.text, &info);
					cout << "File size: " << (int)info.st_size << endl;
					
					if ((file_to_get = open(request.text, O_RDONLY)) < 0){
						response.type = ERR;
						strcpy("Either the file doesn't exist or you don't have sufficient privilege to read the file", response.text);
						write(sock, &response, sizeof(response));
						return;
					}
					
					response.type = ACK;
					response.size = info.st_size;								// set the file size. Helps the client know in advance about the file's size.
					response.perm = (info.st_mode >> 6) & 0x07;
					
					write(sock, &response, sizeof(response));
					
					cout << "Start sending " << (int)info.st_size <<" bytes" << endl;
					int count = info.st_size;
					
					while (count > 0){
						int bytes_sent;
						int n = read(file_to_get, file_data, count < 4086? count: 4086);
						cout << n << " bytes sent." << endl;
						if (n < 0){
							cout << "error sending data" << endl;
							exit(1);
						}
						
						bytes_sent = write(sock, file_data, n);
						
						if (bytes_sent < 0 || n != bytes_sent){
							cout << "ERROR while writing to socket" << endl;
							exit(1);
						}
						
						count -= n;
					}
					break;		
				}
	
		case _UCOPY:
				{		
				      cout << "UCOPY request" << endl;
				      char buffer[4086];
				      memset(&buffer, 0, sizeof(buffer));
				      
				      int count = request.size;
				      cout << "request.siz: " << request.size << endl;
				      cout << "request.text: " << request.text << endl;
				      
				      char err[1000];
				      
				      int file_to_write = open(request.text, O_WRONLY|O_CREAT, 5 << 6);
				      if (file_to_write < 0){
					      cout << "Error: cannot create file" << endl;
					      break;
				      }
	      
				      while (count > 0){	// continue looping as long as thers data yet to be sent to client
					      int bytes_wrote = 0;
					      int n = read(sock, buffer, sizeof(buffer));
					      if (n < 0){
						      cout << "Error reading from socket" << endl;
						      exit(1);
					      }

					      bytes_wrote = write(file_to_write, buffer, n);
					      if (bytes_wrote < 0 || bytes_wrote != n){
						      cout << "ERROR writing" << endl;
						      exit(1);
					      }

					      count -= n;
				      }
				      
				      if (count > 0){
					      response.type = ERR;
					      cout << "ERROR occurd" << endl;
					      strcpy(response.text, "Couldn't create the file on server");// Notify client of failure
				      }
				      else response.type = ACK;
				      
				      write(sock, &response, sizeof(response));																
				      break;								
				}
	
		case _BYE:
			cout << "BYE request" << endl;
			exit(0);				// A bye request was mad. EXIT
	}
}



